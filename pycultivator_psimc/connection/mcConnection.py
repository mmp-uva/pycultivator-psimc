"""The Multi-Cultivator Connection module implements a pyCultivator connection to PSI's Multi-Cultivator MC-1000-OD"""

from pycultivator.connection import SerialPacketConnection
from pycultivator_psimc.connection import mcPacket, mcTemplate
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MCConnection(SerialPacketConnection.SerialPacketConnection):
    """A driver class implementing API for PSI MC-1000-OD"""

    _namespace = "serial"

    _default_settings = SerialPacketConnection.SerialPacketConnection.mergeDefaultSettings(
        {
            "port": None,
            "packet.separator": struct.pack("4s", "FMTP")
        }, namespace=_namespace
    )

    LIGHT_STATE = 0
    LIGHT_INTENSITY_VOLTAGE = 1
    LIGHT_INTENSITY_PERCENTAGE = 2
    LIGHT_INTENSITY_ABSOLUTE = 3

    def __init__(self, settings=None, **kwargs):
        super(MCConnection, self).__init__(settings=settings, **kwargs)
        self._seqNr = 0

    def getSequenceNumber(self):
        """Returns the current sequence number"""
        return self._seqNr

    def takeSequenceNumber(self):
        """Increases the sequence number counter and returns the new number"""
        result = int(self.getSequenceNumber())
        # add one
        self._seqNr += 1
        return result

    def reset(self):
        """Reset the sequence counter (and more when implemented)"""
        self._seqNr = 0

    # Behaviour

    def connect(self, port=None):
        result = super(MCConnection, self).connect(port=port)
        if result:
            self.reset()
            self.startCommunication()
        return result

    def send(self, packet):
        """Write packet to Multi-Cultivator"""
        packet.setSequenceNumber(self.takeSequenceNumber())
        return super(MCConnection, self).send(packet)

    def sendAndReceive(self, packet, template, pause=0, attempts=None):
        """Send a packet and subsequently reads the responds using a template

        :param packet:
        :type packet: pycultivator_psimc.connection.mcPacket.MCPacket
        :param template:
        :type template: pycultivator_psimc.connection.mcTemplate.MCTemplate
        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCTemplate
        """
        return super(MCConnection, self).sendAndReceive(packet, template, pause=pause, attempts=attempts)

    def sendAndReceiveOK(self, packet, pause=0, attempts=None):
        """Send a packet and subsequently reads the responds using a template

        :rtype: bool
        """
        t = mcTemplate.MCOkResponse()
        self.sendAndReceive(packet, t, pause=pause, attempts=attempts)
        return t.isOk()


    #
    # Helper commands
    #

    def checkConnection(self):
        """Checks if the controller is connected, and raises a SerialException if not connected"""
        if not self.isConnected():
            raise MCException("Cannot use connection: not open")

    #
    # Protocol commands
    #

    def sendPing(self):
        """Send a ping command and checks if the response is OK

        :rtype: bool
        """
        self.checkConnection()
        return self.sendAndReceiveOK(mcPacket.MCPacket.createPing())

    def startCommunication(self):
        """Starts the connection with 5 start-up commands

        :return: Returns True on success and False on failure
        :rtype: bool
        """
        self.checkConnection()
        result = False
        # set speed to 9600
        self.setBaudRate(9600)
        # configure and open connection
        self.reconfigure(autoOpen=True)
        if self.isConnected():
            # write 5 packets
            for i in range(5):
                result = self.sendPing()
        return result

    def checkWaterLevel(self):
        """Checks if the water level of the water bath is sufficient

        :return: Whether the water level is OK
        :rtype: bool
        """
        self.checkConnection()
        return self._checkWaterLevel()

    def _checkWaterLevel(self):
        """

        BEGIN_CLASS(getWaterLvl, 1110, "")
        END_CLASS

        BEGIN_CLASS(setAirPumpState, 110, "u")
            uint8_t state;
        END_CLASS

        """
        p = mcPacket.MCPacket(1110)
        t = mcTemplate.MCByteResponse()
        self.sendAndReceive(p, t)
        return t.getData() == 1

    def readGasPumpState(self):
        """Determines the state of the gas pump

        :rtype: bool
        """
        self.checkConnection()
        return self._readGasPumpState()

    def _readGasPumpState(self):
        """

        BEGIN_CLASS(getAirPumpState, 1120, "")
        END_CLASS

        BEGIN_CLASS(

        """
        p = mcPacket.MCPacket(1120)
        t = mcTemplate.MCBoolResponse()
        self.sendAndReceive(p, t)
        return t.getData()

    def setGasPumpState(self, state):
        """Toggles the gas pump"""
        self.checkConnection()
        return self._setGasPumpState(state)

    def _setGasPumpState(self, state):
        """

        BEGIN_CLASS(setAirPumpState, 1121, "u")
            uint8_t state;
        END_CLASS

        BEGIN_CLASS(setAirPumpState, 100, "")
        END_CLASS

        """
        p = mcPacket.MCPacket(1121)
        p.addField("state", "B", 1 if state else 0)
        return self.sendAndReceiveOK(p)

    def readTemperature(self):
        """Reads the current temperature of the water bath"""
        self.checkConnection()
        return self._readTemperature()

    def _readTemperature(self):
        """

        BEGIN_CLASS(getWaterLvl, 1003, "")
        END_CLASS

        BEGIN_CLASS(getWaterLvl, 116, "f")
            float temp; float 4
        END_CLASS

        :rtype: None or float
        """
        p = mcPacket.MCPacket(1003)
        t = mcTemplate.MCFloatResponse()
        self.sendAndReceive(p, t)
        return t.getData()

    def readOD(self, channel=0, led=0, repeats=1):
        """Instructs the Multi-Cultivator to perform an Optical Density measurement in one channel

        Note: this function has no measurement protocol, Readings are obtained directly from the Multi-Cultivator

        :param channel: The channel or vessel to measure the OD <0,7>
        :type channel: int
        :param led: The led to use [ 0=680nm, 1=720nm ]
        :type led: int
        :param repeats: Number of measurements to make and calculate the average over <1,N>
        :type repeats: int
        :return: tuple with (signal intensity, background, OD)
        :rtype: tuple
        """
        self.checkConnection()
        self.constrain(channel, 0, 7, "channel")
        self.constrain(led, 0, 1, "led")
        self.constrain(repeats, 0, 100, "repeats")
        return self._readOD(channel=channel, led=led, repeats=repeats)

    def _readOD(self, channel=0, led=0, repeats=1):
        """Internal command responsible for the actual low-level communication.

        BEGIN_CLASS( getODReadingf, 1092, "uuU" )
            uint8_t light;      byte 1
            uint8_t led;        byte 1 (2
            uint16_t repeats;   short 2 (4
        END_CLASS

        BEGIN_CLASS( ODReadingf, 2092, "UUf" )
            uint16_t flash;     short 2
            uint16_t bgr;       short 2 (4
            float od;           float 4 (8
        END_CLASS

        """
        result = (None, None, None)
        p = mcPacket.MCPacket.createCommand(cmd=1092)
        p.addField("channel", mcPacket.MCPacket.FORMAT_UBYTE, channel)
        p.addField("led", mcPacket.MCPacket.FORMAT_UBYTE, led)
        p.addField("repeats", mcPacket.MCPacket.FORMAT_USHORT, repeats)
        t = mcTemplate.MCTemplate(["H", "H", "f"])
        self.sendAndReceive(p, t)
        if t.hasData():
            result = t.getData()
        return result

    def readDeviceVersion(self):
        self.checkConnection()
        p = mcPacket.MCPacket.createCommand(cmd=4)
        t = mcTemplate.MCStringResponse()
        return self.sendAndReceive(p, t).getData()

    def readLightSettings(self, channel):
        """Get the current light settings for a given channel

        :param channel: Vessel for which to get the light settings <0,7>
        :type channel: int
        :return: A tuple with 4 elements ( ON/OFF, Voltage, Eprc, Eabs )
        :rtype: tuple[bool, float, float, float]
        """
        self.checkConnection()
        self.constrain(channel, 0, 7, name="channel")
        return self._readLightSettings(channel)

    def _readLightSettings(self, channel):
        """

        BEGIN_CLASS(getLightSettings, 1021, "u")
            uint8_t channel;    # byte 1
        END_CLASS

        BEGIN_CLASS(lightSettings, 2021, "ufff")
            uint8_t on;         # byte 1
            float	voltage;    # float 4 (5
            float	uEprc;      # float 4 (9
            float	uEabs;      # float 4 (13
        END_CLASS

        :param channel:
        :return: Settings
        :rtype: tuple[bool, float, float, float]
        """
        result = (None, None, None, None)
        p = mcPacket.MCPacket.createCommand(cmd=1021)
        p.addField("channel", "B", channel)
        t = mcTemplate.MCTemplate(["B", "f", "f", "f"])
        self.sendAndReceive(p, t)
        if t.hasData():
            result = t.getData()
        return result

    def setAllLightState(self, state):
        self.checkConnection()
        return self._setLightState(255, state)

    def setLightState(self, channel, state):
        self.checkConnection()
        self.constrain(channel, 0, 7, name="channel")
        return self._setLightState(channel, state)

    def _setLightState(self, channel, state):
        p = mcPacket.MCPacket.createCommand(cmd=1022)
        p.addField("channel", "B", channel)
        p.addField("state", "B", 1 if state else 0)
        return self.sendAndReceiveOK(p)


class FakeMCConnection(
    MCConnection, SerialPacketConnection.FakeSerialPacketConnection
):
    """A fake connection to a PSI MC-1000-OD Device"""

    _channels = 8

    def __init__(self, settings=None, **kwargs):
        super(FakeMCConnection, self).__init__(settings=settings, **kwargs)
        self._retry_max = 2
        self._pumpState = True
        self._targetTemp = 30.0
        self._controlTemp = True
        self._light_settings = []
        for c in range(self._channels):
            # light_settings[channel] = [ state, volt, perc, abs ]
            self._light_settings.append([True, 100, 100, 120])

    def _connect(self, port=None):
        return SerialPacketConnection.FakeSerialPacketConnection._connect(self, port)

    def disconnect(self):
        return SerialPacketConnection.FakeSerialPacketConnection.disconnect(self)

    def _read(self, length=0):
        return SerialPacketConnection.FakeSerialPacketConnection._read(self, length)

    def _write(self, data):
        return SerialPacketConnection.FakeSerialPacketConnection._write(self, data)

    #
    # Protocol commands
    #

    def _sendPing(self):
        return True

    def _readOD(self, channel=0, led=0, repeats=1):
        """
        Directly measure the OD. This will not mess with the pumps, use measureOD instead
        :param channel: The channel or vessel to measure the OD <0,7>
        :type channel: int
        :param led: The led to use [ 0=680nm, 1=720nm ]
        :type led: int
        :param repeats: Number of measurements to make and calculate the average over <1,N>
        :type repeats: int
        :return: tuple with (signal intensity, background, OD)
        :rtype: tuple
        """
        return self._light_settings[channel][self.LIGHT_INTENSITY_ABSOLUTE], 13.0, 3.0

    def _readODDataSize(self):
        return 1024

    def _readDeviceVersion(self):
        return "Fake Version 1.0"

    def _readLightSettings(self, channel=0):
        return self._light_settings[channel]

    def _setLightState(self, channel=0, state=False):
        if channel == 255:
            for c in range(len(self._light_settings)):
                self._light_settings[c][self.LIGHT_STATE] = state
        else:
            self._light_settings[channel][self.LIGHT_STATE] = state
        return True

    def _setLightPercentage(self, channel=0, value=0):
        if channel == 255:
            for c in range(len(self._light_settings)):
                self._light_settings[c][self.LIGHT_INTENSITY_PERCENTAGE] = value
        else:
            self._light_settings[channel][self.LIGHT_INTENSITY_PERCENTAGE] = value
        return True

    def _setLightAbsolute(self, channel=0, value=0):
        if channel == 255:
            for c in range(len(self._light_settings)):
                self._light_settings[c][self.LIGHT_INTENSITY_ABSOLUTE] = value
        else:
            self._light_settings[channel][self.LIGHT_INTENSITY_ABSOLUTE] = value
        return True

    def _setLightVoltage(self, channel=0, value=0):
        if channel == 255:
            for c in range(len(self._light_settings)):
                self._light_settings[c][self.LIGHT_INTENSITY_VOLTAGE] = value
        else:
            self._light_settings[channel][self.LIGHT_INTENSITY_VOLTAGE] = value
        return True

    def _getTempSetting(self):
        return self._controlTemp, self._targetTemp

    def _setTempState(self, state=False):
        self._controlTemp = state
        return True

    def _setTargetTemperature(self, state=False, temp=0.0):
        self._targetTemp = temp
        self._controlTemp = state
        return True

    def _readTemperature(self):
        return self._targetTemp

    def _checkWaterLevel(self):
        return True

    def _setGasPumpState(self, state=False):
        self._pumpState = state
        return True

    def _readGasPumpState(self):
        return self._pumpState

    def _calibrateOD(self):
        return True


class MCException(SerialPacketConnection.SerialPacketConnectionException):
    """"""

    def __init__(self, msg):
        super(MCException, self).__init__(msg)
