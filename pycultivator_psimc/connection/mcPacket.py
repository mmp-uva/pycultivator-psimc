# coding=utf-8
"""Implements writing of bytearrays from objects"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import struct
from pycultivator.connection import Packet, FormPacket


class MCPacket(FormPacket.SandwichFormPacket, Packet.CRCPacket):
    """Class for writing PSI MC compatible packets

    Responsible for creating PSI MC Packets

    Maybe overloaded to model specific FMTP packets

    Overall format of a packet:
        - header -- -seq- -len- -cmd- <-data-> cs
        46 4d 54 50 01 00 02 00 04 00          08

    The header is a fixed class response (overload getHeaderFormats and getHeaderValues)
    The checksum is calculated using writeCRC
    The sequence number will set by the sender.
    The length is calculated from the number of fields and their type
    """

    def __init__(self, cmd, names=None, fmts=None, values=None):
        """Initialise a PSI MC Packet

        :param names: Names of the fields to add to this packet
        :param fmts: The format specifications of the fields to add
        :param values: The values of the fields to add.
        """
        super(MCPacket, self).__init__(names=names, fmts=fmts, values=values)
        self._cmd = cmd
        self._seq_nr = 0

    def getHeaderFormats(self):
        return ["4s"]

    def getHeaderValues(self):
        return ["FMTP"]

    def getSequenceNumber(self):
        return self._seq_nr

    def setSequenceNumber(self, number):
        # we have 16 bits unsigned
        self.constrain(number, 0, (1 << 16)-1, name="number", include=True)
        self._seq_nr = number

    def getCommand(self):
        return self._cmd

    def getDataLength(self):
        result = 2  # length of command
        for fmt in self.getFieldFormats():
            result += self.sizeOfFormat(fmt)
        return result

    def write(self):
        p = bytearray()
        p = self.writeHeader(p=p) # header
        p = self.writeValue("H", self.getSequenceNumber(), p=p)  # seq num
        p = self.writeValue("H", self.getDataLength(), p=p)  # length
        p = self.writeValue("H", self.getCommand(), p=p)  # cmd
        p = self.writeFields(p=p)  # write data
        p = self.writeCRC(p=p)  # write crc
        return p

    @classmethod
    def createCommand(cls, cmd):
        """Create a command packet

        :type cmd: int
        :param cmd: Command code of this packet
        :rtype: pycultivator_psimc.connection.mcPacket.MCPacket
        :return: Packet object with command code and sequence, without data
        """
        result = cls(cmd)
        return result

    @classmethod
    def createPing(cls):
        """ Create a ping packet (command code = 1)

        :return:
        """
        return cls.createCommand(1)
