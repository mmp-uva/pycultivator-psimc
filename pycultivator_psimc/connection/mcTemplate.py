# coding=utf-8
"""Implements reading of bytearrays into objects"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from pycultivator.connection import Template, FormTemplate


class MCTemplate(Template.SandwichTemplate, Template.CRCTemplate):
    """A Basic PSI MC Packet:

    FMTP    - 4s  - header
    01 00   - H   - sequence number
    02 00   - H   - payload length
    04 00   - H   - command
    <-->    - var - data
    01      - B   - crc
    """

    def __init__(self, data_fmts=None):
        super(MCTemplate, self).__init__()
        if data_fmts is None:
            data_fmts = []
        self._data_fmts = data_fmts
        self._seq = None
        self._length = None
        self._cmd = None
        self._crc = None

    def getTemplateFormat(self):
        return ">" + super(MCTemplate, self).getTemplateFormat()

    def getHeaderFormats(self):
        return ["4s", "H", "H", "H"]

    def readHeader(self, data, offset=0):
        header, offset = self.readValue(data, "4s", offset=offset)
        # read sequence
        seq, offset = self.readValue(data, "H", offset=offset)
        self._seq = seq
        # read data length
        length, offset = self.readValue(data, "H", offset=offset)
        self._length = length
        # read command
        cmd, offset = self.readValue(data, "H", offset=offset)
        self._cmd = cmd
        return [header, seq, length, cmd], offset

    def getDataFormats(self):
        return self._data_fmts

    def getDataLength(self):
        return self._length

    def hasDataLength(self):
        return self.getDataLength() is not None

    def getSequenceNumber(self):
        return self._seq

    def hasSequenceNumber(self):
        return self.getSequenceNumber() is not None

    def getCommand(self):
        return self._cmd

    def hasCommand(self):
        return self.getCommand() is not None

    def getCRC(self):
        return self._crc

    def hasCRC(self):
        return self.getCRC() is not None

    def getData(self):
        return self._data

    def hasData(self):
        return len(self.getData()) == len(self.getDataFormats())


class MCResponse(MCTemplate):

    def __init__(self, cmd, fmt=None):
        if fmt is None:
            fmt = []
        else:
            fmt = [fmt]
        super(MCResponse, self).__init__(data_fmts=fmt)
        self._exp_cmd = cmd

    def getExpectedCommand(self):
        return self._exp_cmd

    def check(self, data):
        result = self.isValid(self.getTemplateFormat(), data)
        if result:
            # extract cmd
            cmd = self.readValue(data, "H", offset=8)[0]
            result = cmd == self.getExpectedCommand()
        return result

    def isOk(self):
        return self.getCommand() == self.getExpectedCommand()

    def getData(self):
        result = None
        if self._data is not None and len(self._data) > 0:
            result = self._data[0]
        return result

    def hasData(self):
        return self.getData() is not None


class MCOkResponse(MCResponse):

    def __init__(self):
        super(MCOkResponse, self).__init__(100)


class MCByteResponse(MCResponse):

    def __init__(self):
        super(MCByteResponse, self).__init__(110, "B")


class MCBoolResponse(MCResponse):

    def __init__(self):
        super(MCBoolResponse, self).__init__(102, "B")

    def getData(self):
        result = super(MCBoolResponse, self).getData()
        return self.parseToBool(result)


class MCShortResponse(MCResponse):

    def __init__(self):
        super(MCShortResponse, self).__init__(111, "H")


class MCIntegerResponse(MCResponse):

    def __init__(self):
        super(MCIntegerResponse, self).__init__(112, "I")


class MCFloatResponse(MCResponse):

    def __init__(self):
        super(MCFloatResponse, self).__init__(116, "f")


class MCStringResponse(MCTemplate):

    def __init__(self):
        super(MCStringResponse, self).__init__()

    def readData(self, data, offset=0):
        result = []
        if self.hasDataLength():
            length = self.getDataLength()
            result = self.readValue(data, "{}s".format(length), offset=offset)
        return result
