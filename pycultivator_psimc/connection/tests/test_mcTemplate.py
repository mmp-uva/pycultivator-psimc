# coding=utf-8
"""
Test file to Test SerialPacket.py
"""

from pycultivator_psimc.connection import mcTemplate
from pycultivator.foundation.tests._test import UvaSubjectTestCase
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestMCTemplate(UvaSubjectTestCase):

    _subject_cls = mcTemplate.MCTemplate
    # FMTP SEQ LEN CMD -DATA- CRC
    _packet = bytearray(struct.pack("4sHHHBB", *["FMTP", 0, 0, 0, 1, 255]))
    _packet.append(mcTemplate.MCOkResponse.crc(_packet))
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCTemplate]
        """
        return super(TestMCTemplate, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(["B", "B"])

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCTemplate
        """
        return super(TestMCTemplate, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))

    def test_read(self):
        self.skipTest("Not implemented yet")


class TestMCResponse(TestMCTemplate):

    _subject_cls = mcTemplate.MCResponse
    _packet = bytearray(struct.pack("4sHHHB", *["FMTP", 0, 0, 100, 255]))
    _packet.append(mcTemplate.MCOkResponse.crc(_packet))
    _code = 100

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCResponse]
        """
        return super(TestMCResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(
            self._code, "B"
        )

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCResponse
        """
        return super(TestMCResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())
        self.assertEqual(self.subject.getCommand(), 100)
        self.assertEqual(self.subject.getData(), 255)


class TestMCOkResponse(TestMCResponse):

    _subject_cls = mcTemplate.MCOkResponse
    _packet = bytearray(struct.pack("4sHHH", *["FMTP", 0, 0, 100]))
    _packet.append(mcTemplate.MCOkResponse.crc(_packet))

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCOkResponse]
        """
        return super(TestMCOkResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCOkResponse
        """
        return super(TestMCOkResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())


class TestMCByteResponse(TestMCResponse):

    _subject_cls = mcTemplate.MCByteResponse
    _packet = bytearray(struct.pack("4sHHHB", *["FMTP", 0, 0, 110, 255]))
    _packet.append(mcTemplate.MCOkResponse.crc(_packet))

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCOkResponse]
        """
        return super(TestMCByteResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCOkResponse
        """
        return super(TestMCByteResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())
        self.assertEqual(self.subject.getData(), 255)


class TestMCBoolResponse(TestMCResponse):

    _subject_cls = mcTemplate.MCBoolResponse
    _packet = bytearray(struct.pack("4sHHHB", *["FMTP", 0, 0, 102, 1]))
    _packet.append(mcTemplate.MCOkResponse.crc(_packet))

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCOkResponse]
        """
        return super(TestMCBoolResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCOkResponse
        """
        return super(TestMCBoolResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())
        self.assertTrue(self.subject.getData())


class TestMCShortResponse(TestMCResponse):

    _subject_cls = mcTemplate.MCShortResponse
    _packet = bytearray(struct.pack("<4sHHHH", *["FMTP", 0, 0, 111, 255]))
    _packet.append(mcTemplate.MCShortResponse.crc(_packet))

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCOkResponse]
        """
        return super(TestMCShortResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCOkResponse
        """
        return super(TestMCShortResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))
        # wrong length
        ba = bytearray(struct.pack("4sHHH", *["FMTP", 0, 0, 110]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))
        ba = bytearray(struct.pack("4sHHHB", *["FMTP", 0, 0, 110, 255]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))
        # wrong cmd
        ba = bytearray(struct.pack("4sHHHH", *["FMTP", 0, 0, 110, 1]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())
        self.assertEqual(self.subject.getData(), 255)


class TestMCFloatResponse(TestMCResponse):

    _subject_cls = mcTemplate.MCFloatResponse
    _packet = bytearray(struct.pack("<4sHHHf", *["FMTP", 0, 0, 116, 1.3]))
    _packet.append(mcTemplate.MCFloatResponse.crc(_packet))

    @classmethod
    def getSubjectClass(cls):
        """

        :return:
        :rtype: type[pycultivator_psimc.connection.mcTemplate.MCFloatResponse]
        """
        return super(TestMCFloatResponse, cls).getSubjectClass()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.mcTemplate.MCFloatResponse
        """
        return super(TestMCFloatResponse, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def test_check(self):
        self.assertTrue(self.subject.check(self._packet))
        # wrong length
        ba = bytearray(struct.pack("4sHHH", *["FMTP", 0, 0, 110]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))
        # wrong argument
        ba = bytearray(struct.pack("4sHHHH", *["FMTP", 0, 0, 110, 255]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))
        # wrong cmd
        ba = bytearray(struct.pack("4sHHHf", *["FMTP", 0, 0, 110, 9.9]))
        ba.append(self.subject.crc(ba))
        self.assertFalse(self.subject.check(ba))

    def test_read(self):
        self.subject.read(self._packet)
        self.assertTrue(self.subject.isOk())
        self.assertAlmostEqual(self.subject.getData(), 1.3, delta=0.01)
