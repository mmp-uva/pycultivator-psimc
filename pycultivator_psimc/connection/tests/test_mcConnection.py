# coding=utf-8
"""
Test file to Test SerialPacket.py
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from time import sleep
import os
from pycultivator.connection.tests.test_SerialConnection import TestSerialConnection, SkipTest
from pycultivator_mc.connection import MCConnection, MCPacket


class TestMCConnection(TestSerialConnection):

    _subject_cls = MCConnection.MCConnection
    _cmd_ok = None
    _cmd_od = None

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_psimc.connection.PSIMCConnection.PSIMCConnection
        """

    @classmethod
    def setUpClass(cls):
        result = super(TestSerialConnection, cls).setUpClass()
        cls._port = os.getenv("SERIAL_PORT", None)
        if cls._port is None:
            raise SkipTest("No port given to run unit tests against")
        return result

    def setUp(self):
        TestSerialConnection.setUp(self)
        self._cmd_ok = MCPacket.MCPacket.createCommand(1, 100)
        self._cmd_od = MCPacket.MCPacket.createCommand(1, 1092)
        self._cmd_od.addField(MCPacket.MCPacket.FORMAT_UBYTE)  # channel
        self._cmd_od.addField(MCPacket.MCPacket.FORMAT_UBYTE)  # led
        self._cmd_od.addField(MCPacket.MCPacket.FORMAT_USHORT)  # repeats
        self._cmd_od.setField(0, 0)
        self._cmd_od.setField(1, 1)
        self._cmd_od.setField(2, 3)

    def tearDown(self):
        if self._subject.isConnected():
            # turn off all lights
            self._subject.setAllLightStates(False)
            # turn gas pum on
            self._subject.setPumpState(True)
        super(TestSerialConnection, self).tearDown()

    def test_getSequenceNumber(self):
        self.assertEqual(self.subject.getSequenceNumber(), 0)
        self.test_connect()
        self.assertEqual(self.getSubject().getSequenceNumber(), 0)
        self._subject.send(self._cmd_ok)
        self.assertEqual(self.getSubject().getSequenceNumber(), 1)

    def test_takeSequenceNumber(self):
        self.assertEqual(self.getSubject().getSequenceNumber(), 0)
        self.assertEqual(self.getSubject().takeSequenceNumber(), 0)
        self.assertEqual(self.getSubject().getSequenceNumber(), 1)
        self.assertEqual(self.getSubject().takeSequenceNumber(), 1)

    def test_sendPing(self):
        if not issubclass(self._serial_cls, FakeSerial):
            raise SkipTest("Cannot run test_send without FakeSerial binding")
        self.test_connect()
        p = PsiPacket.PsiPacket.createPing()
        self._subject.send(p)
        self.assertEqual(
            self._subject.getSerial().readFromWriteQueue(p.getByteSize()),
            PsiPacket.PsiPacket.createCommand(0, 1).writeByteArray()
        )

    def test_getODReading(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(0, 2092)
            recp.addData("H", 120).addData("H", 13).addData("f", 3.0)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        result = self._subject.readOD()
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Get OD Reading from REAL MC, I'll wait for a second to let it complete")
            sleep(1)
        self.assertTrue(None not in result)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1092)
            senp.addData("B", 0).addData("B", 0).addData("H", 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
            self.assertEqual(result, (120, 13, 3.0))

    def test_getODDataSize(self):
        pass

    def test_getDeviceVersion(self):
        raise SkipTest("No implementation of this function yet")

    def test_getLightSettings(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(0, 2012)
            recp.addData("b", 1).addData("f", 100).addData("f", 100).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        result = self._subject.getLightSettings()
        self.assertEqual(len(result), 4)
        self.assertTrue(None not in result)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1021)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
            self.assertEqual(result, (True, 100, 100, 120))

    def test_setAllLightStates(self):
        self.assertRaises(ValueError, self._subject.setAllLightStates)
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self.assertTrue(self._subject.setAllLightStates(True))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1022)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 255).addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(0, 2012)
            recp.addData("b", 1).addData("f", 100).addData("f", 100).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertTrue(self._subject.getLightSettings()[0])
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # turn all lights off
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self.assertTrue(self._subject.setAllLightStates(False))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1022)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 255).addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(0, 2012)
            recp.addData("b", 0).addData("f", 100).addData("f", 100).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertFalse(self._subject.getLightSettings()[0])

    def test_setLightState(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setLightState(0, True))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1022)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0).addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Light ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("b", 1).addData("f", 100).addData("f", 100).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertTrue(self._subject.getLightSettings()[0])

    def test_setAllLightsPercentage(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setAllLightsPercentage(25))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1023)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 255).addData(PsiPacket.PsiPacket.FORMAT_FLOAT, 25)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("b", 1).addData("f", 25).addData("f", 25).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertEqual(self._subject.getLightSettings()[2], 25)

    def test_setLightPercentage(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setLightPercentage(0, 25))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1023)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0).addData(PsiPacket.PsiPacket.FORMAT_FLOAT, 25)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("B", 1).addData("f", 25).addData("f", 25).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertEqual(self._subject.getLightSettings()[2], 25)

    def test_setAllLightsAbsolute(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setAllLightsAbsolute(100))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1024)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 255).addData(PsiPacket.PsiPacket.FORMAT_FLOAT, 100)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("B", 1).addData("f", 25).addData("f", 25).addData("f", 100)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertAlmostEqual(self._subject.getLightSettings()[3], 100, delta=5)

    def test_setLightAbsolute(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setLightAbsolute(0, 100))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1024)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0).addData(PsiPacket.PsiPacket.FORMAT_FLOAT, 100)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("B", 1).addData("f", 25).addData("f", 25).addData("f", 100)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertAlmostEqual(self._subject.getLightSettings()[3], 100, delta=5)

    def test_setLightVoltage(self):
        self.test_connect()
        self._subject.setAllLightStates(True)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        # set light state in channel 0 to True
        self.assertTrue(self._subject.setLightVoltage(0, 100))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1025)
            senp.addData(PsiPacket.PsiPacket.FORMAT_UBYTE, 0).addData(PsiPacket.PsiPacket.FORMAT_FLOAT, 100)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Lights ON in REAL MC, I'll wait for a second to show off")
            sleep(1)
        # test light settings of channel 0
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(1, 2012)
            recp.addData("B", 1).addData("f", 100).addData("f", 100).addData("f", 120)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        self.assertAlmostEqual(self._subject.getLightSettings()[1], 100, delta=5)

    def test_getTempSetting(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            recp = PsiPacket.PsiPacket.createCommand(0, 2000).addData("B", 1).addData("f", 30.0)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        result = self._subject.getTempSetting()
        self.assertTrue(None not in result)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1000)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_setTempState(self):
        self.test_connect()
        # turn temperature control on
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self.assertTrue(self._subject.setTempState(True))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1001).addData("B", 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        # check temperature control state
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            recp = PsiPacket.PsiPacket.createCommand(1, 2000).addData("B", 1).addData("f", 30.0)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        result = self._subject.getTempSetting()
        self.assertTrue(result[0])
        # now turn temperature control off
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(1).writeByteArray())
        self.assertTrue(self._subject.setTempState(False))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1001).addData("B", 0)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_setTargetTemperature(self):
        self.test_connect()
        # set temperature control on and to 20
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self.assertTrue(self._subject.setTargetTemperature(True, 20))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1002). \
                addData("B", 1).addData("f", 20)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        # check temperature control state
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            recp = PsiPacket.PsiPacket.createCommand(1, 2000).addData("B", 1).addData("f", 20.0)
            self._subject.getSerial().addToReadQueue(recp.writeByteArray())
        result = self._subject.getTempSetting()
        self.assertAlmostEqual(result[1], 20, delta=5)
        # now turn temperature control off and to 30
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(1).writeByteArray())
        self.assertTrue(self._subject.setTargetTemperature(False, 30))
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1002). \
                addData("B", 0).addData("f", 30)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_getTemperature(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createFloat(0, 25).writeByteArray())
        self.assertAlmostEqual(self._subject.getTemperature(), 25, delta=10)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1003)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_checkWaterLevel(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createByte(0, 1).writeByteArray())
        self._log.info("Water level is%s OK" % "" if self._subject.checkWaterLevel() else " NOT")
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1110)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_setPumpState(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self._subject.setPumpState(False)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1121).addData("B", 0)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Pump OFF in REAL MC, I'll wait for a second to let it complete")
            sleep(1)
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self._subject.setPumpState(True)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1121).addData("B", 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )

    def test_getPumpState(self):
        self.test_connect()
        # check pump state
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createByte(0, 1).writeByteArray())
        self._log.info("Gas pump is%s active" % "" if self._subject.getPumpState() else " NOT")
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1120)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        self._subject.getPumpState()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(1).writeByteArray())
        self._subject.setPumpState(True)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1121).addData("B", 1)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )
        # check pump state
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().clearReadQueue()
            self._subject.getSerial().clearWriteQueue()
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createByte(3, 1).writeByteArray())
        self.assertTrue(self._subject.getPumpState())

    def test_calibrateOD(self):
        self.test_connect()
        if issubclass(self._serial_cls, FakeSerial):
            self._subject.getSerial().addToReadQueue(PsiPacket.PsiPacket.createOK(0).writeByteArray())
        self._log.info("Blanking the OD sensor")
        self._subject.calibrateOD()
        if not issubclass(self._serial_cls, FakeSerial):
            self._log.info("Running blanking on REAL MC, I'll sleep for 20 seconds to allow it to complete")
            sleep(20)
        if issubclass(self._serial_cls, FakeSerial):
            senp = PsiPacket.PsiPacket.createCommand(self._subject.getSequenceNumber() - 1, 1093)
            self.assertEqual(
                self._subject.getSerial().readFromWriteQueue(senp.getByteSize()), senp.writeByteArray()
            )


class TestFakePsiController(TestMCConnection):

    _subject_cls = MCConnection.FakeMCConnection

    @classmethod
    def setUpClass(cls):
        return super(TestMCConnection, cls).setUpClass()

    def test_read(self):
        self.assertTrue(self.getSubject().connect())
        self.getSubject().addToReadQueue(self._cmd_ok.writeByteArray())
        self.assertEqual(self.getSubject().read(self._cmd_ok), self._cmd_ok, msg="test_receive #1")
        self._cmd_ok.setSequenceNumber(5)
        self.getSubject().addToReadQueue(self._cmd_ok.writeByteArray())
        self.assertEqual(self.getSubject().read(self._cmd_ok), self._cmd_ok, msg="test_receive #2")
        self.getSubject().addToReadQueue(self._cmd_od.writeByteArray())
        self.assertEqual(self.getSubject().receive(self._cmd_od), self._cmd_od, msg="test_receive #3")
        with self.assertRaises(MCConnection.MCException, msg="test_receive #4"):
            self.getSubject().read(self._cmd_ok)

    def test_write(self):
        self.assertTrue(self.getSubject().connect())
        self.getSubject().write(self._cmd_ok)  # should have seqNr = 1
        p = MCPacket.MCPacket.createCommand(1, 100)
        self.assertEqual(
            self.getSubject().readFromWriteQueue(self._cmd_ok.getByteSize()), p.writeByteArray())
        with self.assertRaises(MCConnection.MCException):
            self.getSubject().write(MCPacket.MCPacket.createCommand())

    def test_reset(self):
        self.assertTrue(self.getSubject().connect())
        self.getSubject().write(self._cmd_ok)
        self.assertEqual(self._cmd_ok.getSequenceNumber(), 0)
        self.getSubject().write(self._cmd_ok)
        self.assertEqual(self._cmd_ok.getSequenceNumber(), 1)
        self.getSubject().reset()
        self.getSubject().write(self._cmd_ok)
        self.assertEqual(self._cmd_ok.getSequenceNumber(), 0)

    def test_calibrateOD(self):
        with self.assertRaises(MCConnection.MCException):
            self.getSubject().calibrateOD()
        # TODO: Expand
