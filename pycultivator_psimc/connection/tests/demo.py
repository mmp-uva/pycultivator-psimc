"""A small demo script for quickly testing the connection"""

from pycultivator_psimc.connection import mcConnection
from pycultivator_lab.script import script
import argparse
import time

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class DemoScript(script.Script):

    def __init__(self):
        super(DemoScript, self).__init__()
        self._cultivator = None
        self._port = None
        self._fake = False

    def getPort(self):
        return self._port

    def setPort(self, port):
        self._port = port

    def hasPort(self):
        return self.getPort() is not None

    def fakesConnection(self):
        return self._fake is True

    def fakeConnection(self, state):
        self._fake = state is True

    def getCultivator(self):
        """ Return the device used by this script

        :return:
        :rtype: pycultivator_psimc.connection.mcConnection.MCConnection
        """
        return self._cultivator

    def hasCultivator(self):
        return self.getCultivator() is not None

    def loadCultivator(self):
        if self.fakesConnection():
            self.getLog().info("Simulate connection to Multi-Cultivator")
            result = mcConnection.FakeMCConnection()
        else:
            result = mcConnection.MCConnection()
        self._cultivator = result
        return result

    def _prepare(self):
        result = super(DemoScript, self)._prepare()
        if not self.hasCultivator():
            self.loadCultivator()
        if self.hasPort():
            self.getCultivator().connect(self.getPort())
        result = self.getCultivator().isConnected() and result
        self.getLog().info("Connected to cultivator: {}".format(result))
        return result

    def execute(self):
        # check water level
        has_water = self.getCultivator().checkWaterLevel()
        if not has_water:
            self.getLog().warning("Not enough water in the cultivator!")
        temp = self.getCultivator().readTemperature()
        if temp is not None:
            self.getLog().info("Current temperature of bath: {:.2f}".format(temp))
        else:
            self.getLog().warning("Unable to read temperature")
        state = self.getCultivator().readGasPumpState()
        self.getLog().info("Gas pump is {}".format("ON" if state else "OFF"))
        # stop gas
        self.getCultivator().setGasPumpState(False)
        # sleep
        self.getLog().info("Sleep for 2 seconds")
        time.sleep(2)
        # turn light off
        self.getLog().info("Turn light OFF")
        self.getCultivator().setAllLightState(False)
        # measure od
        led = 1
        for c in range(8):
            reading = self.getCultivator().readOD(c, led, 1)
            self.getLog().info(
                "Channel {}, LED {} | "
                "Flash = {:.3f} Background = {:.3f} OD = {:.3f}".format(c, led, *reading)
            )
        # turn light on
        self.getLog().info("Turn light ON")
        self.getCultivator().setAllLightState(True)
        # restore gas flow
        self.getLog().info("Turn gas pump ON")
        self.getCultivator().setGasPumpState(True)
        return True

    def _clean(self):
        if self.getCultivator().isConnected():
            self.getCultivator().disconnect()
        result = not self.getCultivator().isConnected()
        self.getLog().info("Disconnect from cultivator: {}".format(result))
        # clean rest up after we disconnect
        result = super(DemoScript, self)._clean() and result
        return result

    def default_arguments(self):
        defaults = super(DemoScript, self).default_arguments()
        defaults["port"] = self.getPort()
        defaults["fake"] = self.fakesConnection()
        return defaults

    def define_arguments(self, parser=None):
        if parser is None:
            parser = argparse.ArgumentParser(description="Demo application of Reglo Pump")
        parser = super(DemoScript, self).define_arguments(parser)
        parser.add_argument(
            "port", help="Set port to which the pump is connected"
        )
        parser.add_argument(
            "-s", action="store_true", dest="fake",
            help="Fake the connection"
        )
        return parser

    def parse_arguments(self, arguments):
        super(DemoScript, self).parse_arguments(arguments)
        self.setPort(arguments.get("port", self.getPort()))
        self.fakeConnection(arguments.get("fake", False))

if __name__ == "__main__":
    ds = DemoScript()
    # create argument parser
    p = ds.define_arguments()
    # load defaults
    d = ds.default_arguments()
    # parse arguments and overwrite defaults
    d.update(vars(p.parse_args()))
    # read arguments into script
    ds.parse_arguments(d)
    # run script
    ds.run()
