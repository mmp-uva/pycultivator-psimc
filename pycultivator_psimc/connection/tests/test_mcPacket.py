# coding=utf-8
"""
Test file to Test SerialPacket.py
"""

from pycultivator_psimc.connection import mcPacket
from pycultivator.foundation.tests._test import UvaSubjectTestCase
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestMCPacket(UvaSubjectTestCase):

    _subject_cls = mcPacket.MCPacket
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """

        :rtype: type[pycultivator_psimc.connection.mcPacket.MCPacket]
        """
        return super(TestMCPacket, cls).getSubjectClass()

    def getSubject(self):
        """

        :rtype: pycultivator_psimc.connection.mcPacket.MCPacket
        """
        return super(TestMCPacket, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(
            100
        )

    def test_write(self):
        ba = bytearray(struct.pack(
            "4sHHH", *["FMTP", 0, 2, 100]
        ))
        ba.append(self.subject.crc(ba))
        self.assertEqual(self.subject.write(), ba)
        self.subject.addField("a", "H", 3)
        ba = bytearray(struct.pack(
            "4sHHHH", *["FMTP", 0, 4, 100, 3]
        ))
        ba.append(self.subject.crc(ba))
        self.assertEqual(self.subject.write(), ba)
