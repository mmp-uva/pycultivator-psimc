"""The PSI Multi-Cultivator provides bindings to PSI's Multi-Cultivator (MC-1000-OD)"""

from pycultivator.device import Device
from pycultivator_psimc.instrument import mcODSensor, mcLight

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MultiCultivator(Device.ComplexDevice):
    """ The LightCalibrationPlate device """

    _namespace = "psimc"

    _default_settings = Device.Device.mergeDefaultSettings(
        {

        }, namespace=_namespace
    )

    MAX_CHANNELS = 48

    def __init__(self, name, parent, settings=None, **kwargs):
        super(MultiCultivator, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_psimc.connection.mcConnection.MCConnection
        """
        return super(MultiCultivator, self).getConnection()

    def createConnection(self, fake=False, **kwargs):
        """Creates a new connection and uses it for this device"""
        from pycultivator_psimc.connection import mcConnection
        if not fake:
            self.setConnection(mcConnection.MCConnection(**kwargs))
        else:
            self.setConnection(mcConnection.FakeMCConnection(**kwargs))
        return self.getConnection()

    def getChannels(self):
        """ Returns all the wells known by this device

        :rtype: dict[int, pycultivator_psimc.device.MultiCultivator.PSIMultiCultivatorChannel]
        """
        return super(MultiCultivator, self).getChannels()

    def getChannel(self, idx):
        """ Returns the well at the given index

        :rtype: pycultivator_psimc.device.MultiCultivator.PSIMultiCultivatorChannel
        """
        return super(MultiCultivator, self).getChannel(idx)

    # Behaviour methods

    def connect(self, fake=False):
        """Connect to the Light Calibration Plate"""
        result = False
        if not self.hasConnection():
            self.createConnection(fake=fake)
        if self.hasConnection():
            result = self.getConnection().connect()
        return result

    def control(self, variable, value, **kwargs):
        if variable == mcLight.MCLight.VARIABLE_INTENSITY:
            results = self.setAllLightIntensity(value)
        elif variable == mcLight.MCLight.VARIABLE_LIGHT_INTENSITY_VOLTAGE:
            results = self.setALLLightIntensityVoltage(value)
        elif variable == mcLight.MCLight.VARIABLE_STATE:
            value = self.parseValue(value, vtype=bool, default=False)
            results = self.setAllLightState(value)
        else:
            results = super(MultiCultivator, self).control(variable, value, **kwargs)
        return results

    def setAllLightIntensity(self, value, store=False):
        """Sets the light intensity in all channels"""
        results = []
        # issue command
        c = self.getConnection()
        c.setAllLightsAbsolute(value)
        # update channels
        for c in self.getChannels().values():
            c.getLight().setIntensity(value)
        return results

    def setALLLightIntensityVoltage(self, value):
        """Sets the light intensity of all channels in voltage"""
        results = []
        # issue command
        c = self.getConnection()
        c.setAllLightsPercentage(value)
        # update channels
        return results

    def setAllLightState(self, state):
        """Sets the light state in all channels"""
        results = []
        # issue command
        # update channels
        return results


class PSIMultiCultivatorChannel(Device.DeviceChannel):
    """A channel in a Multi-Cultivator"""

    _namespace = "mc.channel"

    _default_settings = Device.DeviceChannel.mergeDefaultSettings(
        {

        }, namespace=_namespace
    )

    INSTRUMENT_OD = "od"
    INSTRUMENT_LIGHT = "light"

    def __init__(self, name, parent, settings=None, **kwargs):
        super(PSIMultiCultivatorChannel, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    # Getters // Setters

    def getDevice(self):
        """ Returns the Multi-Cultivator object to which this channel belongs.

        :return: The plate object
        :rtype: pycultivator_psimc.device.MultiCultivator.MultiCultivator
        """
        return super(PSIMultiCultivatorChannel, self).getDevice()

    def addInstrument(self, i):
        """Registers an instrument under it's own name to the channel.

        Only allows 1 Light Panel and 1 OD Sensor to be registered to this channel

        :type i: pycultivator_psimc.instrument.mcInstrument.MCInstrument
        """
        name = i.getName()
        kind = i.getTypeSynonyms()
        if self.hasInstrumentOfClass(type(i)):
            raise PSIMultiCultivatorException(
                "A Multi-Cultivator can only contain one instrument of type - {}".format(type(i))
            )
        if name is not None:
            self.getInstruments()[name] = i
            i.setParent(self)
        return self

    def hasLight(self):
        """Returns whether there is a Light Panel Instrument registered to this channel

        :rtype: bool
        """
        return self.getLight() is not None

    def getLight(self):
        """Returns the Light Panel that is registered to this channel

        :return:
        :rtype: None or pycultivator_psimc.instrument.mcLight.MCLight
        """
        instruments = self.getInstrumentsOfType(self.INSTRUMENT_LIGHT).values()
        return instruments[0] if len(instruments) > 0 else None

    def hasODSensor(self):
        """Returns whether this channel has an OD Sensor to registered

        :rtype: bool
        """
        return self.getODSensor() is not None

    def getODSensor(self):
        """Returns the OD Sensor that is registered to this channel

        :return:
        :rtype: None or pycultivator_psimc.instrument.mcODSensor.MCODSensor
        """
        instruments = self.getInstrumentsOfType(self.INSTRUMENT_OD).values()
        return instruments[0] if len(instruments) > 0 else None

    # Behaviour methods

    def setLightIntensity(self, value):
        """Sets the intensity (in uE/m2/s) of the LED that provides the given color for this well

        :type value: Light intensity in uE/m2/s
        """
        panel = self.getLight()
        if panel is None:
            raise PSIMultiCultivatorException("No Light Panel found in channel {}".format(self.getChannelIndex()))
        return panel.setIntensity(value)

    def setLightState(self, state):
        """Sets the state of the LED that provides the given color for this well

        :type state: bool
        """
        panel = self.getLight()
        if panel is None:
            raise PSIMultiCultivatorException("No Light Panel found in channel {}".format(self.getChannelIndex()))
        return panel.setState(state is True)


class PSIMultiCultivatorException(Device.DeviceException):
    """An exception raised by the LightCalibrationPlate class"""

    pass
