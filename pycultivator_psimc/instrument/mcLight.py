"""
The PSIMCLight Module provides classes for working with LED Panels in a PSI Multi-Cultivator
"""

import mcInstrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MCLight(mcInstrument.MCInstrument):
    """The MCLight class models the LED Panel in a Channel in the PSI Multi-Cultivator"""

    _namespace = "psimc.instrument.light"
    INSTRUMENT_TYPE = mcInstrument.MCInstrument.INSTRUMENT_TYPE_LIGHT
    INSTRUMENT_TYPE_SYNONYMS = [INSTRUMENT_TYPE]

    _default_settings = mcInstrument.MCInstrument.mergeDefaultSettings({
        "state": True,      # bool
        "voltage": 100.0,   # in voltage
    }, namespace=_namespace)

    VARIABLE_LIGHT_INTENSITY_VOLTAGE = "light_intensity_voltage"

    VARIABLES_MEASURED = [
        mcInstrument.MCInstrument.VARIABLE_INTENSITY,
        mcInstrument.MCInstrument.VARIABLE_STATE,
        VARIABLE_LIGHT_INTENSITY_VOLTAGE
    ]

    VARIABLES_CONTROLLED = [
        mcInstrument.MCInstrument.VARIABLE_INTENSITY,
        mcInstrument.MCInstrument.VARIABLE_STATE,
        VARIABLE_LIGHT_INTENSITY_VOLTAGE
    ]

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(MCLight, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    def getIntensity(self, update=False):
        """Returns the light intensity (in uE/m2/s) that is currently emitted by the pane

        :param update: Whether to refresh the internal settings
        :type update: bool
        :return: The intensity in uE/m2/s (as known by the object)
        :rtype: float
        """
        if update:
            self._readLightSettings(store=True)
        return self.calibrate(self._getVoltage())

    def setIntensity(self, value, write=False):
        """Sets the light intensity (in ue/m2/s) of the LED Panel

        :param value: The intensity to be applied
        :type value: float
        :param write: Whether to write the setting to the Multi-Cultivator
        :type write: bool
        :return: The new intensity in uE/m2/s (as known by the object)
        :rtype: float
        """
        self._setVoltage(self.inverse_calibrate(value))
        if write:
            self._writeLightSettings()
        return self.getIntensity()

    def _getVoltage(self):
        """Return the voltage applied to the LED Panel

        :rtype: float
        """
        return self.getSetting("voltage")

    def _setVoltage(self, value):
        """Sets the voltage applied to the LED panel

        :type value: float
        :rtype: float
        """
        self.setSetting("voltage", value)
        return self._getVoltage()

    def getState(self, update=False):
        """Return the state of the LED-Panel

        :param update: Whether to refresh the internal settings
        :type update: bool
        :return: The state of the LED (as known by the object)
        :rtype: bool
        """
        if update:
            self._readLightSettings(store=True)
        return self._getState()

    def setState(self, state, write=False):
        """Sets the state of the LED Panel

        :param state: The state to set the Panel to
        :type state: bool
        :param write: Whether to write this to the Multi-Cultivator
        :type write: bool
        :return: The new state of the panel (as known by the object)
        :rtype: bool
        """
        self._setState(state is True)
        if write:
            self._writeLightSettings()
        return self.getState()

    def _getState(self):
        return self.getSetting("state") is True

    def _setState(self, state):
        self.setSetting("state", state is True)
        return self._getState()

    def _readLightVoltage(self, store=True):
        """Reads the voltage that is being applied by the Multi-Cultivator

        NOTICE: This method will retrieve both voltage and state from the Multi-Cultivator but will ignore the state.

        :param store: Whether to write the voltage to the memory of the object
        :type store: bool
        :rtype: float
        """
        voltage = self._readLightSettings(store=False)[1]
        if store:
            self._setVoltage(voltage)
        return voltage

    def _readLightState(self, store=True):
        """Reads the state of the LED Panel in the Multi-Cultivator

        NOTICE: This method will retrieve both voltage and state from the Multi-Cultivator but will ignore the voltage.

        :param store: Whether to write the state to the memory of the object
        :type store: bool
        :rtype: bool
        """
        state = self._readLightSettings(store=False)[0]
        if store:
            self._setState(state)
        return state

    def _readLightSettings(self, store=True):
        """Reads both the applied voltage and state of the LED Panel in the Multi-Cultivator

        NOTICE: This method is a efficient way of reading both state and voltage.
            Calling readLight...() methods independently will cause two packets to be sent instead of one.

        :param store: Whether to write the settings to the device
        :type store: bool
        :rtype: tuple[bool, float]
        """
        c = self.getDevice().getConnection()
        state, v, perc, abs = c.getLightSettings(self.getChannelIndex())
        if store:
            self._setState(state is True)
            self._setVoltage(v)
        return state, v

    def _writeLightSettings(self, voltage=None, state=None):
        """Writes the Light Settings to the Multi-Cultivator

        :param voltage: The voltage to be set. If None; load voltage from object
        :type voltage: float
        :param state: The state to be set. If None; load the state from object
        :type state: bool
        :rtype: bool
        """
        result = self._writeLightState(state=state)
        result = self._writeLightVoltage(voltage=voltage) and result
        return result

    def _writeLightState(self, state=None):
        """Sets the state of the LED-Panel in the Multi-Cultivator

        :param state: The state to be set. If None; load the state from object
        :type state: bool
        :rtype: bool
        """
        if state is None:
            state = self._getState()
        return self.getDevice().getConnection().setLightState(self.getAddress(), state)

    def _writeLightVoltage(self, voltage=None):
        """Sets the voltage that will be applied to the LED-Panel in the Multi-Cultivator

        :param voltage: The voltage to be set. If None; load voltage from object
        :type voltage: float
        :rtype: bool
        """
        if voltage is None:
            voltage = self._getVoltage()
        return self.getDevice().getConnection().setLightVoltage(self.getAddress(), voltage)

    def measure(self, variable, **kwargs):
        """Measures the LED-Panel: i.e. the Intensity as a Light record

        :rtype: None or pycultivator_psimc.data.DataModel.LightRecord
        """
        result = None
        if variable == self.VARIABLE_LIGHT_INTENSITY:
            result = self.getIntensity(update=True)
        if variable == self.VARIABLE_LIGHT_STATE:
            result = self.getState(update=True)
        return result

    def control(self, variable, value, **kwargs):
        """Controls the intensity of the LED-Panel in the given unit.

        If unit == "state" (LIGHT_UNIT_STATE): sets the state of the LED Panel (0-1)
        If unit == "voltage" (LIGHT_UNIT_VOLTAGE): sets the voltage of the LED Panel (0-100)
        If unit == "intensity" (LIGHT_UNIT_INTENSITY): sets the intensity of the LED Panel (>= 0)

        :rtype: bool
        """
        result = False
        if variable == self.VARIABLE_LIGHT_INTENSITY:
            result = self.setIntensity(value)
        if variable == self.VARIABLE_LIGHT_STATE:
            result = self.setState(self.parseToBool(value, False))
        return result


class PSIMCLightException(mcInstrument.PSIMCInstrumentException):
    """An exception raised by a PSI Multi-Cultivator light"""

    def __init__(self, msg):
        super(PSIMCLightException, self).__init__(msg)
