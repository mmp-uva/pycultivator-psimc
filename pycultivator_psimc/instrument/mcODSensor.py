"""
The PSIMCLight Module provides classes for working with LED Panels in a PSI Multi-Cultivator
"""

import mcInstrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MCODSensor(mcInstrument.MCInstrument):
    """The MCLight class models the LED Panel in a Channel in the PSI Multi-Cultivator"""

    _namespace = "psimc.instrument.od"
    INSTRUMENT_TYPE = mcInstrument.MCInstrument.INSTRUMENT_TYPE_OD
    INSTRUMENT_TYPE_SYNONYMS = [INSTRUMENT_TYPE, "od_sensor", "odsensor"]

    _default_settings = mcInstrument.MCInstrument.mergeDefaultSettings({
        "wavelength": 720,
        "repeats": 3,
    }, namespace=_namespace)

    VARIABLES_MEASURED = [mcInstrument.MCInstrument.VARIABLE_OD]
    VARIABLES_CONTROLLED = []

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(MCODSensor, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    def getWavelength(self):
        """Returns the wavelength (in nm) at which this instrument measures the OD"""
        return self.getSetting("wavelength")

    def _setWavelength(self, nm):
        """Sets the wavelength (in nm) at which this instruments meausres the OD

        :param nm: The intensity to be applied
        :type nm: float
        """
        self.setSetting("wavelength", nm)
        return self.getWavelength()

    def getRepeats(self):
        """Return the voltage applied to the LED Panel

        :rtype: float
        """
        return self.getSetting("repeats")

    def setRepeats(self, repeats):
        """Sets the number of subsequent reading that the Multi-Cultivator takes"""
        self.setSetting("repeats", repeats)
        return self.getRepeats()

    def getAddress(self):
        """Returns the address of at which this instrument can be reached

        :rtype: tuple[int, int]
        """
        super(MCODSensor, self).getAddress()

    def setAddress(self, address):
        """Sets the address at which this instrument can be reached

        :type address: tuple[int, int]
        :rtype: tuple[int, int]
        """
        if address is not None and not isinstance(address, (list, tuple)) and len(address) != 2:
            raise PSIMCODException("Invalid address - {} - received. Expected a tuple of length 2".format(address))
        self.setSetting("address", address)
        return self.getAddress()

    def _read(self, repeats=None):
        """Reads the OD using this instrument

        NOTICE: This method assumes that lights are turned off and the culture has been cleared of bubbles.
            These things should be prepared by a protocol.

        :param repeats: Number of subsequent readings the Multi-Cultivator will collect and average
        :type repeats: int
        :return: Tuple with signal intensity, background intensity and od value
        :rtype: tuple[float, float, float]
        """
        if repeats is None:
            repeats = self.getRepeats()
        channel, led = self.getAddress()
        return self.getDevice().getConnection().readOD(channel, led, repeats)

    def guessAddress(self):
        """Guesses the address of the OD Sensor: i.e. the index of the channel

        :return: The channel index
        :rtype: int
        """
        return self.getChannelIndex(), 1 if self.getWavelength() == 720 else 0

    def measure(self, variable):
        """Measures the OD Sensor

        :rtype: None or pycultivator_psimc.data.DataModel.LightRecord
        """
        result = None
        if variable == self.VARIABLE_OD:
            result = self._read()
        return result

    def control(self, variable, value):
        result = False
        # nothing to control
        return result


class PSIMCODException(mcInstrument.PSIMCInstrumentException):
    """An exception raised by a PSI Multi-Cultivator light"""

    def __init__(self, msg):
        super(PSIMCODException, self).__init__(msg)
