"""
The PSIMCInstrument Module provides basic classes for working with Instruments in a Multi-Cultivator
"""

from pycultivator.instrument import Instrument

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class MCInstrument(Instrument.Instrument):
    """The MCInstrument class models an Instrument in a Multi-Cultivator"""

    _namespace = "psimc.instrument"

    _default_settings = Instrument.Instrument.mergeDefaultSettings({
        "address": None,
    }, namespace=_namespace)

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(MCInstrument, self).__init__(name=name, parent=parent, settings=settings, **kwargs)

    # Getters / Setters

    def getDevice(self):
        """Returns the device object to which this instrument belongs.

        :rtype: pycultivator_psimc.device.MultiCultivator.MultiCultivator
        """
        return self.getChannel().getDevice()

    def getChannel(self):
        """Returns the channel object to which this instrument belongs.

        :rtype: pycultivator_psimc.device.MultiCultivator.PSIMultiCultivatorChannel
        """
        if not self.hasParent():
            raise PSIMCInstrumentException("Instrument does not have a parent")
        return self.getParent()

    def getChannelIndex(self):
        """Returns the index of the channel in the Multi-Cultivator

        :rtype: int
        """
        if not self.hasParent():
            raise PSIMCInstrumentException("Instrument does not have a parent")
        return self.getParent().getChannelIndex()

    def getAddress(self):
        address = self._getAddress()
        if address is None:
            self.guessAddress()
        return address

    def hasAddress(self):
        return self._getAddress() is not None

    def _getAddress(self):
        return self.getSetting("address")

    def setAddress(self, address):
        self.setSetting("address", address)
        return self.getAddress()

    def guessAddress(self):
        return self.getChannelIndex()

    # Behaviour methods

    def measure(self, variable, **kwargs):
        """Measures the instrument

        :param variable: The variable to measure
        :param kwargs: Extra arguments passed
        :rtype: pycultivator.data.DataModel.Record
        """
        raise NotImplementedError

    def control(self, variable, value, **kwargs):
        """Controls the instrument

        :param variable: The variable to control
        :type variable: str
        :param value: The value to set the variable to
        :type value: int | float | bool
        :param kwargs: Extra arguments passed.
        :type kwargs: dict[str, int | float | bool | str]
        :return: Whether it succeeded
        :rtype: bool
        """
        raise NotImplementedError


class PSIMCInstrumentException(Instrument.InstrumentException):
    """Exception raised by a Multi-Cultivator Instrument"""

    def __init__(self, msg):
        super(PSIMCInstrumentException, self).__init__(msg)
