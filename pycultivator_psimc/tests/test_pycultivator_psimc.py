"""A global test script to test the functioning of the whole package"""

import _test

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PackageTestCase(_test.LivePSIMCTestCase):
    """Tests the package"""

    ALL_CHANNELS = range(8)
    TEST_LED_INTENSITY = 1000

    def test_leds(self):
        """Test if we can set every LED"""
        # turn on
        for channel in self.ALL_CHANNELS:
            msg = "Channel: #{}, LED ON".format(channel)
            with self.subTest(msg):
                self.assertTrue(self.apply_light(channel, self.TEST_LED_INTENSITY))
        # turn off
        for channel in self.ALL_CHANNELS:
            msg = "Channel: #{}, LED OFF".format(channel)
            with self.subTest(msg):
                self.assertTrue(self.apply_light(channel, 0))

    def test_od(self):
        for channel in self.ALL_CHANNELS:
            msg = "Channel: #{}, OD".format(channel)
            with self.subTest(msg):
                self.assertIsNotNone(self.measure_od(channel))


    def apply_light(self, channel, intensity):
        return self.getCultivator().getConnection().setLightAbsolute(
            channel, intensity
        )

    def measure_od(self, channel):
        return self.getCultivator().getConnection().getODReading(
            channel
        )
