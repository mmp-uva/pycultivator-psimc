"""Core test classes for a PSI Multi-Cultivator

Handles basic configuration
"""

import os
import sys

import pkg_resources
from pycultivator.foundation.tests import _test
from pycultivator_psimc.config import xmlConfig

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class LivePSIMCTestCase(_test.UvaTestCase):
    """TestCase class for live functionality tests, these tests require a working plate"""

    PSIMC_CONFIG_PATH = pkg_resources.resource_filename(
        __name__, os.path.join("..", "config", "schema", "configuration.xml")
    )

    _use_fake = os.getenv("USE_FAKE", "1") == "1"
    _cultivator = None

    @staticmethod
    def normalize(signals, blanks):
        results = []
        if not isinstance(signals, (tuple, list)):
            signals = [signals]
        if not isinstance(blanks, (tuple, list)):
            blanks = [blanks]
        for idx, signal in enumerate(signals):
            blank = 0.0 if idx > len(blanks) else blanks[idx]
            results.append(signal - blank)
        return results

    @classmethod
    def loadPSIMC(cls, path=None, settings=None, **kwargs):
        """ Loads the configuration object and device from the given XML Configuration file path into a new script

        :param path: Path to the XML Configuration file
        :type path: str
        :param settings: Settings to be passed to the configuration
        :type settings: None or dict
        :return:
        :rtype: pycultivator_psimc.device.PSIMultiCultivator.PSIMultiCultivator
        """
        result = False
        if path is None:
            path = cls.PSIMC_CONFIG_PATH
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_plate_lcp.device.PlateLCP.PlateLCP"""
            if d is not None:
                cls.setCultivator(d)
                result = cls.hasCultivator()
        return result

    @classmethod
    def getCultivator(cls):
        """Return the plate object or nothing if no plate is set

        :rtype: pycultivator_psimc.device.PSIMultiCultivator.PSIMultiCultivator or None
        """
        return cls._cultivator

    @classmethod
    def hasCultivator(cls):
        """Returns whether a plate is set for this test

        :rtype: bool
        """
        return cls.getCultivator() is not None

    @classmethod
    def setCultivator(cls, plate):
        """Sets the plate object that is used in this test

        :type plate: pycultivator_psimc.device.PSIMultiCultivator.PSIMultiCultivator
        """
        cls._cultivator = plate
        return cls.getCultivator()

    @classmethod
    def usesFake(cls):
        """Whether this test uses a fake plate connection, thus simulating a plate"""
        return cls._use_fake

    @classmethod
    def getChannels(cls):
        """Return a list of wells in the plate, if a plate is set

        :rtype: None or list[pycultivator_psimc.device.PSIMultiCultivator.PSIMultiCultivatorChannel]
        """
        result = None
        if cls.hasCultivator():
            result = cls.getCultivator().getChannels().values()
        return result

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(LivePSIMCTestCase, cls).setUpClass()
        # see if a fake connection should be made
        cls.getLog().info(
            "Will {} simulate the connection to the Multi-Cultivator".format(
                "NOT" if cls.usesFake() else ""
            )
        )
        # load configuration classes
        cls.loadPSIMC(settings={"fake.connection": cls.usesFake()})
        if not cls.hasCultivator():
            raise AssertionError("Unable to load configuration file")
        result = cls.getCultivator().connect()
        if not result:
            raise AssertionError("Unable to connect to Multi-Cultivator")

    @classmethod
    def tearDownClass(cls):
        if cls.getCultivator().isConnected():
            result = cls.getCultivator().disconnect()
            if not result:
                raise AssertionError("Unable to disconnect from Multi-Cultivator")

    @classmethod
    def report(cls, msg, end="\n"):
        sys.stderr.write("{}{}".format(msg, end))
        sys.stderr.flush()
